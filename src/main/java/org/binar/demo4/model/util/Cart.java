package org.binar.demo4.model.util;

import lombok.Data;
import org.binar.demo4.model.Produk;

import java.util.List;
import java.util.Map;

@Data
public class Cart {

    private Map<Produk, Long> produkQty;

}
