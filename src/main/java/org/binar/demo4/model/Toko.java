package org.binar.demo4.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Toko {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idToko;

    private String namaToko;

    private String alamatToko;

    private Time jamBuka;

    private Time jamTutup;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "idTransaksi")
    private List<Transaksi> idTransaksi;
}
