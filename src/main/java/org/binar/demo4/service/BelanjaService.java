package org.binar.demo4.service;

import org.binar.demo4.model.util.Cart;

public interface BelanjaService {
    String belanja(Cart cart);
}
