package org.binar.demo4.service;

import org.binar.demo4.model.Toko;
import org.binar.demo4.repository.TokoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Service
public class TokoServiceImpl implements TokoService{

    @Autowired
    TokoRepository tokoRepository;

    @Override
    public List<Toko> findTokoOpen(LocalTime when) {
        return tokoRepository.findTokoOpen(when);
    }

}
