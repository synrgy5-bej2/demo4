package org.binar.demo4.service;

import org.binar.demo4.model.Toko;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public interface TokoService {

    List<Toko> findTokoOpen(LocalTime when);

}
