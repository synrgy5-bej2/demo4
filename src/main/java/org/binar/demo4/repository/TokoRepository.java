package org.binar.demo4.repository;

import org.binar.demo4.model.Toko;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface TokoRepository extends JpaRepository<Toko, Long> {

    @Query(nativeQuery = true, value = "select * from toko where jam_buka <= :when and jam_tutup >= :when")
    public List<Toko> findTokoOpen(@Param("when") LocalTime when);

}
