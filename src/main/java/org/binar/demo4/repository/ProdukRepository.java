package org.binar.demo4.repository;

import org.binar.demo4.model.Produk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProdukRepository extends JpaRepository<Produk, UUID> {
}
