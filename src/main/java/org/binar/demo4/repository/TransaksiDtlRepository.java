package org.binar.demo4.repository;

import org.binar.demo4.model.TransaksiDtl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TransaksiDtlRepository extends JpaRepository<TransaksiDtl, UUID> {
}
