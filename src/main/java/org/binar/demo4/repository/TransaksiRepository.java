package org.binar.demo4.repository;

import org.binar.demo4.model.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransaksiRepository extends JpaRepository<Transaksi, UUID> {

    @Query(value = "select * from transaksi tx " +
            "join toko tk on tx.id_toko = tk.id_toko " +
            "where tx.id_toko = :idToko",
            nativeQuery = true)
    List<Transaksi> findTransaksi(@Param("idToko") Long idToko);
}
