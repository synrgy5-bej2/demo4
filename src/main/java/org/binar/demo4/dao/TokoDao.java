package org.binar.demo4.dao;

import org.binar.demo4.model.Toko;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class TokoDao extends DaoUtil{

    public List<Toko> getTokoMasihBuka(String jamCheck) throws SQLException {
        String query = "select * from toko where jam_buka <= ? and jam_tutup >= ?";
        List<Toko> tokoList = new ArrayList<>();

        // try with resources
        try(Connection conn = DriverManager.getConnection(conUrl, uname, pass);
            PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setTime(1, Time.valueOf(jamCheck));
            ps.setTime(2, Time.valueOf(jamCheck));

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                // nama toko | alamat
//                System.out.println("pake columnLabel");
//                System.out.println(rs.getString("nama_toko") + "\t|\t" +
//                        rs.getString("alamat_toko"));
//
//                System.out.println("\n=================\n");

                System.out.println("pake columnIndex");
                System.out.println(rs.getString(5) + "\t|\t" +
                        rs.getString(2));
//                Toko toko = new Toko(rs.getLong(1), rs.getString(5), rs.getString(2),
//                        rs.getTime(3), rs.getTime(4));
//                tokoList.add(toko);
            }
            return tokoList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer addTokoBaru(Toko toko) {
        return null;
    }
}
