package org.binar.demo4.dao;

import org.binar.demo4.model.Transaksi;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransaksiDao extends DaoUtil{

    public List<Transaksi> getTransaksiByIdToko(Long idToko) {
        String query = "select * from transaksi tx " +
                "join toko tk on tk.id_toko = tx.id_toko where tx.id_toko = ?";
        try(Connection conn = DriverManager.getConnection(conUrl, uname, pass);
            PreparedStatement ps = conn.prepareStatement(query)) {
            ps.setLong(1, 2l);

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                System.out.println(":::::: Struk Belanja ::::::" +
                        "\n" + rs.getString("nama_toko") +
                        "\n" + rs.getString("alamat_toko") +
                        "\n\n" + rs.getTimestamp("waktu_transaksi") +
                        "\n\n" + rs.getString("id_transaksi") +
                        "\n" + rs.getLong("total_harga"));
            }
            return null;
        } catch (SQLException sqe) {
//            throw new SQLException();
            sqe.printStackTrace();
            return new ArrayList<>();
        }
    }
}
