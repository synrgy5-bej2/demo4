package org.binar.demo4.dao;

import org.springframework.beans.factory.annotation.Value;

public class DaoUtil {
    @Value("${spring.datasource.url}")
    protected String conUrl;

    @Value("${spring.datasource.username}")
    protected String uname;

    @Value("${spring.datasource.password}")
    protected String pass;
}
