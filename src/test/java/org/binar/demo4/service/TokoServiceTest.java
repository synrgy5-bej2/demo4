package org.binar.demo4.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.LocalTime;

@SpringBootTest
public class TokoServiceTest {

    @Autowired
    TokoService tokoService;

    @Test
    void testTokoBuka() {
        tokoService.findTokoOpen(LocalTime.now()).forEach(val -> {
            System.out.println(val.getNamaToko() + " | " + val.getAlamatToko());
        });
    }
}
