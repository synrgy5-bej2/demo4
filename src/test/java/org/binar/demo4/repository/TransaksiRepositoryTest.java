package org.binar.demo4.repository;

import org.binar.demo4.model.Toko;
import org.binar.demo4.model.Transaksi;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootTest
public class TransaksiRepositoryTest {

    @Autowired
    TransaksiRepository transaksiRepository;

    @Autowired
    TokoRepository tokoRepository;

    @Test
    void saveTransaksi() {
        Toko toko = new Toko();
        toko.setIdToko(3l);
        List<Toko> tokoList = new ArrayList<>();

        Transaksi trx = new Transaksi();
        trx.setWaktuTransaksi(LocalDateTime.now());
        trx.setTotalHarga(1000001l);
        trx.setIdToko(toko);

        transaksiRepository.save(trx);
    }

    @Test
    void getTransaksiXToko() {
        List<Transaksi> transaksiList = transaksiRepository.findTransaksi(2L);
        transaksiList.forEach(val -> {
            System.out.println(":::::: invoice ::::::: " +
                    "\ntotal harga : " + val.getTotalHarga() +
                    "\nwaktu transaksi : " + val.getWaktuTransaksi() +
                    "\nnama toko : " + val.getIdToko().getNamaToko() +
                    "\nalamat toko : " + val.getIdToko().getAlamatToko());
        });
    }

    @Test
    void getTransaksiXTokoCaraBodoh() {
        Transaksi trx = transaksiRepository.findById(UUID.fromString("c0a80108-8485-12dc-8184-85f2f0650000"))
                .orElseGet(null);
        Toko toko = tokoRepository.findById(trx.getIdToko().getIdToko()).orElseGet(null);
        System.out.println(":::::: invoice ::::::: " +
                "\ntotal harga : " + trx.getTotalHarga() +
                "\nwaktu transaksi : " + trx.getWaktuTransaksi() +
                "\nnama toko : " + toko.getNamaToko() +
                "\nalamat toko : " + toko.getAlamatToko());
    }
}
