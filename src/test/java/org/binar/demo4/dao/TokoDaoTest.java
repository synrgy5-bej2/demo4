package org.binar.demo4.dao;

import org.binar.demo4.model.Toko;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.SQLException;
import java.sql.Time;
import java.util.List;

@SpringBootTest
public class TokoDaoTest {

    @Autowired
    TokoDao tokoDao;

    @Test
    void testApapun() {

    }

    @ParameterizedTest
    @ValueSource(strings = {"06:00:00", "21:00:00"})
    void testTokoMasihBuka(String jam) throws SQLException {
        List<Toko> tokoList =  tokoDao.getTokoMasihBuka(jam);
        if(Time.valueOf(jam).after(Time.valueOf("18:00:00"))) {
            Assertions.assertEquals(2, tokoList.size());
            Assertions.assertTrue(tokoList.stream()
                    .anyMatch(val -> val.getAlamatToko().equals("Tebet")));
        } else {
            Assertions.assertEquals(1, tokoList.size());
            Assertions.assertTrue(tokoList.stream()
                    .anyMatch(val -> val.getAlamatToko().equals("Monas")));
        }
    }
}
